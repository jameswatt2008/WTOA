<?php

if (!defined('THINK_PATH'))
    exit();
// 系统别名定义文件
return array(
    'PHPMailer' => WEB_ROOT . 'Common/Extend/PHPMailer/phpmailer.class.php',
    'CheckCode' => WEB_ROOT . 'Common/Extend/CheckCode/Checkcode.class.php',
    'QRCode' => WEB_ROOT . '/Common/Extend/QRCode.class.php',
    'Category' => WEB_ROOT . '/Common/Extend/Category.class.php',
    'SysCrypt' => WEB_ROOT . '/Common/Extend/SysCrypt.class.php',
    'nusoap' => LIB_PATH . '/Ext/nusoap.class.php',
    
    'phprpc' => LIB_PATH . '/Ext/phprpc_server.class.php',
    'xml' => LIB_PATH . '/Ext/xml.class.php',
    'SoapDiscovery' => LIB_PATH . '/Ext/SoapDiscovery.class.php',
    
    
    'CnvpSeverSSO' => LIB_PATH . '/Ext/CnvpSeverSSO.class.php',
);