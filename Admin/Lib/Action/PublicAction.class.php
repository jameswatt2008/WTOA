<?php

class PublicAction extends Action {

    /**
      +----------------------------------------------------------
     * 初始化
      +----------------------------------------------------------
     */
    public function _initialize() {
        header("Content-Type:text/html; charset=utf-8");
        header('Content-Type:application/json; charset=utf-8');
    }    
    public function login() {
        session_destroy();unset($_COOKIE);
        if (IS_POST) {
            isset($_SESSION) && session_destroy();
            isset($_COOKIE) && $_COOKIE=null;
            isset($_POST['sid'])&& session_id($_POST['sid']);
            session_start();
            //表单令牌验证
            $returnLoginInfo = D("Public")->auth();
            //生成认证条件
            if ($returnLoginInfo['status'] == 1) {
                $map = array();
                // 支持使用绑定帐号登录
                $map['username'] = $this->_post('username');
                import('ORG.Util.RBAC');
                $authInfo = RBAC::authenticate($map);
                $_SESSION[C('USER_AUTH_KEY')] = $authInfo['id'];
                $_SESSION['username'] = $authInfo['username'];
                if ($authInfo['username'] == C('ADMIN_AUTH_KEY')) {
                    $_SESSION[C('ADMIN_AUTH_KEY')] = true;
                }
                // 缓存访问权限
                RBAC::saveAccessList();
            }
            exit(json_encode($returnLoginInfo));
        } else {            
            $cookie = explode("_", $_COOKIE['sid']);
            session_id($cookie[0]); 
            if (isset($_COOKIE['sid']) && $cookie[0] ==  session_id()) 
                $this->redirect("Index/index"); 
            
            $systemConfig = include WEB_ROOT . 'Common/systemConfig.php';
            $this->assign("site", $systemConfig);
            $this->display("Common:login");
        }
    }

    public function loginOut() {
        setcookie('sid', NULL, -3600, "/");
        unset($_SESSION['sid'], $_COOKIE['sid']);
        if (isset($_SESSION[C('USER_AUTH_KEY')])) {
            unset($_SESSION[C('USER_AUTH_KEY')]);
            unset($_SESSION);
            session_destroy();
        }
        $this->redirect("Index/index");
    }

    public function findPwd() {
        $M = M("Admin");
        if (IS_POST) {
            $this->checkToken();
            echo json_encode(D("Public")->findPwd());
        } else {
            setcookie('sid', NULL, -3600, "/");
            unset($_SESSION['sid'], $_COOKIE['sid']);
            $cookie = $this->_get('code');
            $shell = substr($cookie, -32);
            $aid = (int) str_replace($shell, '', $cookie);
            $info = $M->where("`aid`='$aid'")->find();
            if ($info['status'] == 0) {
                $this->error("你的账号被禁用，有疑问联系管理员吧", __APP__);
            }
            if (md5($info['find_code']) == $shell) {
                $this->assign("info", $info);
                $_SESSION['aid'] = $aid;
                $systemConfig = include WEB_ROOT . 'Common/systemConfig.php';
                $this->assign("site", $systemConfig);
                $this->display("Common:findPwd");
            } else {
                $this->error("验证地址不存在或已失效", __APP__);
            }
        }
    }

}