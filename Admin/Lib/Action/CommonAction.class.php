<?php

class CommonAction extends Action {

    public $loginMarked;
    //初始化
    public function _initialize() {
        header("Content-Type:text/html; charset=utf-8");
        // header('Content-Type:application/json; charset=utf-8');
        $systemConfig = include WEB_ROOT . 'Common/systemConfig.php';
        if (empty($systemConfig['TOKEN']['admin_marked'])) {
            $systemConfig['TOKEN']['admin_marked'] = "QQ:511693597";
            $systemConfig['TOKEN']['admin_timeout'] = 3600;
            $systemConfig['TOKEN']['member_marked'] = "http://www.cnvp.cn";
            $systemConfig['TOKEN']['member_timeout'] = 3600;
            F("systemConfig", $systemConfig, WEB_ROOT . "Common/");
            if (is_dir(WEB_ROOT . "install/")) {
                delDirAndFile(WEB_ROOT . "install/", TRUE);
            }
        }
        $this->checkLogin();
        // 用户权限检查,是否需要验证
        if (C('USER_AUTH_ON') && !in_array(MODULE_NAME, explode(',', C('NOT_AUTH_MODULE')))) {
            import('ORG.Util.RBAC');
            if (!RBAC::AccessDecision()) {
                //检查认证识别号
                if (!$_SESSION [C('USER_AUTH_KEY')]) {
                    //跳转到认证网关
                    redirect(C('USER_AUTH_GATEWAY'));
                    //redirect(PHP_FILE . C('USER_AUTH_GATEWAY'));
                }
                // 没有权限 抛出错误
                if (C('RBAC_ERROR_PAGE')) {
                    // 定义权限错误页面
                    redirect(C('RBAC_ERROR_PAGE'));
                } else {
                    if (C('GUEST_AUTH_ON')) {
                        $this->assign('jumpUrl', C('USER_AUTH_GATEWAY'));
                    }
                    // 提示错误信息
                    //echo L('_VALID_ACCESS_');
                    $this->error(L('_VALID_ACCESS_'));
                }
            }
        }
        $this->assign("my_info", $_SESSION['my_info']);
        $this->show_sub_menu();
        $this->getPosArr();
    }
    protected function getPosArr(){
        $r['model'] = M('Node')->where("level=2 and name='".MODULE_NAME."'")->getField('title');
        $pid = M('Node')->where("level=2 and name='".MODULE_NAME."'")->getField('id');
        $r['action'] = M('Node')->where("level=3 and pid=$pid  and name='".ACTION_NAME."'")->getField('title');
        $r['nav'] = $r['model'].(empty($r['action'])?'':' \ '.$r['action']);
        $this->assign('pagepos', $r);     
    }
    //登陆验证
    public function checkLogin() {
        if (isset($_COOKIE['sid'])) {
            
            $cookie = explode("_", $_COOKIE['sid']);
            $timeout = C("TOKEN");
            if (time() > (end($cookie) + $timeout['admin_timeout'])) {
                setcookie("sid", NULL, -3600, "/");
                unset($_SESSION['sid'], $_COOKIE['sid']);
                $this->error("登录超时，请重新登录", U("Public/login"));
            } else {
                if ($cookie[0] == session_id()) {
                    setcookie("sid", $cookie[0] . "_" . time(), 0, "/");
                } else {                    
                    setcookie("sid", NULL, -3600, "/");
                    unset($_SESSION['sid'], $_COOKIE['sid']);
                    $this->error("帐号异常，请重新登录", U("Public/login"));
                }
            }
        } else {
            $this->redirect("Public/login");
        }
        return TRUE;
    }

    //令牌验证
    protected function checkToken() {
        if (IS_POST) {
            if (!M("Admin")->autoCheckToken($_POST)) {
                die(json_encode(array('status' => 0, 'info' => '令牌验证失败')));
            }
            unset($_POST[C("TOKEN_NAME")]);
        }
    }
    /**
      +----------------------------------------------------------
     * 显示二级菜单
      +----------------------------------------------------------
     */
    public function show_sub_menu() {
        $big = MODULE_NAME == "Index" ? "Common" : MODULE_NAME;
        $cache = C('admin_sub_menu');
        $sub_menu = array();
        if ($cache[$big]) {
            $cache = $cache[$big];
            foreach ($cache as $url => $title) {
                $url = $big == "Common" ? $url : "$big/$url";
                $sub_menu = array('url' => U("$url"), 'title' => $title);
            }
            $this->assign("sub_menu", $sub_menu);
        } else {
            $sub_menu = array('url' => '#', 'title' => "该菜单组不存在"); 
            $this->assign("sub_menu", $sub_menu);
        }
    }

}