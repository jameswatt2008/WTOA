<?php

import('SSO/.xml');
import('SSO/CnvpServerSSO');

// <editor-fold defaultstate="collapsed" desc="公开的WebService函数">
class UserService {

    //用户中心 登陆用户处理
    public function loginUCenter($username, $password, $ip, $siteFlag="", $remember=FALSE) {
        $ret = array();
        //TODO 远程验证码,暂时不启用 -zx
//        if (!isValidChecksum($username . $password . $ip . $siteFlag . $remember, $checksum)) {
//            $ret['resultFlag'] = CHECK_KEY_ERROR;
//            return xml_serialize($ret);
//        }
        $ret = CnvpServerSSO::loginUCenter($username, $password, $ip, $siteFlag, $remember);
        return xml_serialize($ret);
    }

    //获取当前在线的某个用户(需要通过客户端保存的session id存根 来获取)
    public function getOnlineUser($sessId, $siteFlag, $checksum) {
        $ret = array();
        if (!isValidChecksum($sessId . $siteFlag, $checksum)) {
            $ret['resultFlag'] = CHECK_KEY_ERROR;
            return xml_serialize($ret);
        }
        $ret = CnvpServerSSO::getOnlineUser($sessId, $siteFlag);
        return xml_serialize($ret);
    }

    //获取当前在线的某个用户(需要通过客户端保存的session id存根 来获取)
    public function getOnlineUserDetail($sessId, $siteFlag, $fields, $checksum) {
        $ret = array();
        if (!isValidChecksum($sessId . $siteFlag, $checksum)) {
            $ret['resultFlag'] = CHECK_KEY_ERROR;
            return xml_serialize($ret);
        }
        $ret = CnvpServerSSO::getOnlineUserDetail($sessId, $siteFlag, $fields);
        return xml_serialize($ret);
    }

    // 单点登出处理
    public function logoutUCenter($sessId, $siteFlag, $checksum) {
        $ret = array();
        if (!isValidChecksum($sessId . $siteFlag, $checksum)) {
            $ret['resultFlag'] = CHECK_KEY_ERROR;
            return xml_serialize($ret);
        }

        $ret['resultFlag'] = 1;
        $ret['script'] = CnvpServerSSO::fetchLogoutScript($sessId, $siteFlag);
        CnvpServerSSO::logoutUCenter($sessId);
        return xml_serialize($ret);
    }

    //根据用户名获取用户密码，邮箱，性别，生日，地址
    public function getUser($username, $checksum) {
        $ret = array();
        if (!isValidChecksum($username, $checksum)) {
            $ret['resultFlag'] = "-1";
            return xml_serialize($ret);
        }
        if (!isValidUsername($username)) {
            $ret['resultFlag'] = "-2";
            return xml_serialize($ret);
        }
        $res_get = uc_get_user($username, 'username, password, email, sex, userbirthday, location');
        //don't exist this user
        if ($res_get == 0) {
            $ret['resultFlag'] = "-3";
            return xml_serialize($ret);
        }
        $ret = $res_get;
        $ret['resultFlag'] = "1";
        return xml_serialize($ret);
    }

}

// </editor-fold>
class ApiAction extends Action {

    public function index() {
        $saveLogs = false;
        header("Content-type: text/xml");
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            // TODO: 这里需要改下-zx
            if ($saveLogs)
                $this->saveLog('request', $GLOBALS["HTTP_RAW_POST_DATA"]);
            $servidorSoap = new SoapServer('http://localhost/index.php/Api/index?wsdl');
            $servidorSoap->setClass('UserService');
            $servidorSoap->handle();
            $response = ob_get_contents();
            if ($saveLogs)
                $this->saveLog('response', $response);
        } else {
            import('@.Ext.SoapDiscovery');
            $disco = new SoapDiscovery('UserService', 'CNVP');
            if (isset($_SERVER['QUERY_STRING']) && strcasecmp($_SERVER['QUERY_STRING'], 'wsdl') >= 0)
                echo $disco->getWSDL();
            else
                echo $disco->getDiscovery();
        }
    }

    public function saveLog($prefix, $content) {
        $fdir = APP_PATH . '\\log\\';
        if (!file_exists($fdir))
            mkdir($fdir);
        $fpath = $fdir . $prefix . '_' . time() . '.xml';
        $of = fopen($fpath, 'w'); //创建并打开dir.txt
        if ($of) {
            fwrite($of, $content); //把执行文件的结果写入txt文件
        }
        fclose($of);
    }

}
