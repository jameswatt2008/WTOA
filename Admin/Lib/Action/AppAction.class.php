﻿<?php

class AppAction extends CommonAction {
    public function index(){
        $this->assign("list", D("App")->appList());
        $this->display();
    }
    public function add(){
         if (IS_POST) {
            $this->checkToken();
            header('Content-Type:application/json; charset=utf-8');
            echo json_encode(D("App")->appAdd());
        } else{
            $this->display('edit');            
        }
    }
    public function edit(){
         if (IS_POST) {
            $this->checkToken();
            header('Content-Type:application/json; charset=utf-8');
            echo json_encode(D("App")->appEdit());
        } else{
              $M = M("App");
            $info = $M->where("id=" . (int) $_GET['id'])->find();
            if (empty($info['id'])) {
                $this->error("不存在该应用", U('App/index'));
            }
            $this->assign("info",$info);
            $this->display();   
        }
    }
    
    public function del(){
        header('Content-Type:application/json; charset=utf-8');
        echo json_encode(D("App")->appDel());
    }    
    public function ping(){
        header('Content-Type:text/javascript; charset=utf-8');
        $appid = intval($_GET['appid']);
        $data['id'] = $appid;
        $app = M('App')->where($where)->find();
        if ($app) {
            $return =  file_get_contents($app['url'].'/sso.php?page=urltest&key='.$app['token']);            
            if($return == '1') {
                echo 'document.getElementById(\'status_' . $appid . '\').innerHTML = "<img src=\'/Public/images/correct.gif\' border=\'0\' class=\'statimg\' \/><span class=\'green\'>通信成功</span>";testlink();';
            } else {
                echo 'document.getElementById(\'status_' . $appid . '\').innerHTML = "<img src=\'/Public/images/error.gif\' border=\'0\' class=\'statimg\' \/><span class=\'red\'>通信失败</span>";testlink();';
            }
        }
    }
}

?>
