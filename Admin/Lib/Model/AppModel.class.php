<?php

class AppModel extends Model {
    
    public function appList() {
        import("Category");
        $cat = new Category('App', array('id', 'pid', 'title', 'fullname'));
        $temp = $cat->getList();               //获取分类结构
        $level = array("1" => "项目（GROUP_NAME）", "2" => "模块(MODEL_NAME)", "3" => "操作（ACTION_NAME）");
        foreach ($temp as $k => $v) {
            $temp[$k]['statusTxt'] = $v['status'] == 1 ? "√" : "禁用";
            $temp[$k]['chStatusTxt'] = $v['status'] == 0 ? "启用" : "禁用";
            $temp[$k]['level'] = $level[$v['level']];
            $list[$v['id']] = $temp[$k];
        }
        unset($temp);
        return $list;
    }
    public function appEdit() {
        $M = M("App");
        return $M->save($_POST) ? array('status' => 1, info => '更新成功', 'url' => U('App/index')) : array('status' => 0, info => '更新失败');
    }

    public function appAdd() {
        $M = M("App");
        return $M->add($_POST) ? array('status' => 1, info => '添加成功', 'url' => U('App/index')) : array('status' => 0, info => '添加失败');
    }
    public function appDel() {
        $M = M("App");
        $w['id'] = (int) $_GET["id"];
        return $M->where($w)->delete() ? array('status' => 1, info => '删除成功') : array('status' => 0, info => '删除失败');
    }
}

?>
