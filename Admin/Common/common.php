<?php
function saveLog($prefix, $content) {
    $fdir = APP_PATH . '\\log\\';
    if (!file_exists($fdir))
        mkdir($fdir);
    $fpath = $fdir . $prefix . '_' . time() . '.xml';
    $of = fopen($fpath, 'w'); //创建并打开dir.txt
    if ($of) {
        fwrite($of, $content); //把执行文件的结果写入txt文件
    }
    fclose($of);
}