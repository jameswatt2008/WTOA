<?php

require_once 'HelloWorld.class.php';
function saveLog($prefix,$content) {       
            $fdir = '.\\log\\';
            if (!file_exists($fdir))
                mkdir($fdir);
            $fpath = $fdir . $prefix.'-'.time(). '.xml';
            $of = fopen($fpath, 'w'); //创建并打开dir.txt
            if ($of) {
                fwrite($of, $content); //把执行文件的结果写入txt文件
            }
            fclose($of);
    }
// Enciende el servidor o despliega WSDL
if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
    ob_start();
    $servidorSoap = new SoapServer('http://localhost/wsdltest/HelloWorldService.php?wsdl');
    $servidorSoap->setClass('HelloWorld');
    $servidorSoap->handle(); 
    $response = ob_get_contents();    
    saveLog('request', $GLOBALS["HTTP_RAW_POST_DATA"]);
    saveLog('response', $response);
} else {
    require_once 'SoapDiscovery.class.php';

    // Crea el servidor de descubrimiento
    $disco = new SoapDiscovery('HelloWorld', 'CNVP');
    header("Content-type: text/xml");
    if (isset($_SERVER['QUERY_STRING']) && strcasecmp($_SERVER['QUERY_STRING'], 'wsdl') == 0) {
        echo $disco->getWSDL();
    } else {
        echo $disco->getDiscovery();
    }
}
?>