<?php

return array(
    'URL_MODEL' => 1,
    'URL_CASE_INSENSITIVE' => TRUE,
    'URL_HTML_SUFFIX' => '',
        /* 错误设置 */
    'ERROR_MESSAGE'         => '页面错误！请~~~稍后再试～',//错误显示信息,非调试模式有效
    'ERROR_PAGE'            => '',	// 错误定向页面
    'SHOW_ERROR_MSG'        => true,    // 显示错误信息
    'TRACE_EXCEPTION'       => false,   // TRACE错误信息是否抛异常 针对trace方法 
);
?>