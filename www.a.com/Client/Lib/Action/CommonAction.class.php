<?php

class CommonAction extends Action {

    public function _initialize() {
        import('SSO/CnvpClientSSO');
        $checkInfo = CnvpClientSSO::checkUserLogin();
        if ($checkInfo['status']== -3) {
            $this->error('身份验证失败或过期，请重新登陆',U('Public/login'));
        }
    }

}